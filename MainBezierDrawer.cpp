/*
 OpenGL visualization skeleton for displaying bitmap images. Just provide a GenerateImage function.
 Good starting point for all image processing exercises for parallel programming.
*/

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <gl\GLU.h>
#include <gl\glut.h>
#include <math.h>

#include "Types.h"

#define SCALE 1
#define ROTATE 2
#define TRANSLATE 3

#define AXIS_SWAP(x) TEX_SIZE-(x)
#define SIGN(x) ((x < 0) ? -1 : ((x > 0) ? 1 : 0)) 

float matrix2d[3][3] = {{1,0,256},{0,1,256},{0,0,1}};								// x, y, w
float matrix3d[4][4] = {{1,0,0,256},{0,1,0,256},{0,0,1,0},{0,0,0,1}};				// x, y, z, w
int initMatrix[3][3] = {{1,0,256},{0,1,256},{0,0,1}};

GLuint texture;
Pixel image[TEX_SIZE][TEX_SIZE];

Vertex vertexField[50];

int endIndex1;
float width, heigth = 0;

int transformations = SCALE;
int dyt = 0;
int dxt = 0;
long double angle = 0;

void ColorizePixel(Pixel *pixel,int red, int green, int blue)
{
	pixel->red = red <= 255 ? (red < 0 ? 0 : red) : 255;
	pixel->green = green <= 255 ? (green < 0 ? 0 : green) : 255;
	pixel->blue = blue <= 255 ? (blue < 0 ? 0 : blue) : 255;
}

void matrix2dMultiplication(float matrix[3][3])
{
	int i,j,k;
	float tmpMatrix[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			for (k = 0; k < 3; k++)
			{
				tmpMatrix[i][j] += matrix2d[i][k] * matrix[k][j];
			}
		}
	}
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			matrix2d[i][j] = tmpMatrix[i][j];
		}
	}
}

void ClearImage() {
	int i,j;

	for (i = 0; i < TEX_SIZE; i++)
	{
		for (j = 0; j < TEX_SIZE; j++)
		{
			ColorizePixel(&image[i][j],0,0,0);
		}
	}
}

void scale()
{
	if (dxt != 0 && dyt != 0)
	{
		float matrix[3][3] = {{1+dxt/width,0,0},{0,1+dyt/heigth,0},{0,0,1}};
		matrix2dMultiplication(matrix);	
	}
	else
	{
		if (dxt != 0)
		{
			float matrix[3][3] = {{1+dxt/width,0,0},{0,1,0},{0,0,1}};
			matrix2dMultiplication(matrix);	
		}
		if (dyt != 0)
		{
			float matrix[3][3] = {{1,0,0},{0,1+dyt/heigth,0},{0,0,1}};
			matrix2dMultiplication(matrix);	
		}
	}
}


void translate() 
{
	if (dxt != 0)
	{
		matrix2d[0][2] += dxt;
	}
	if (dyt != 0) 
	{
	matrix2d[1][2] += dyt;
	}
}

void rotate()
{
	float tmpMatrix[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
	float output[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
	float matrix[3][3] = {{cos(angle),-sin(angle),0},{sin(angle),cos(angle),0},{0,0,1}};
	int i,X,Y,W,j,k;
	if (angle!=0)
	{
		for (i = 0; i < 3; i++)
		{
			for (j = 0; j < 3; j++)
			{
				for (k = 0; k < 3; k++)
				{
					output[i][j] += matrix[k][j]*matrix2d[i][k];
				}
			}
		}
		for (i = 0; i < 3; i++)
		{
			for (j = 0; j < 3; j++)
			{
				matrix2d[i][j] = output[i][j];
			}
		}
	}
}

void reset()
{
	int i,j;
	dxt = 0;
	dyt = 0;
	angle = 0;
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			matrix2d[i][j] = initMatrix[i][j];
		}
	}
}

void DrawLine(Vertex A, Vertex B)
{
	int i,dx,dy,sdx,sdy,dxabs,dyabs,x,y,px,py;

	dx=B.x-A.x;      /* the horizontal distance of the line */
	dy=B.y-A.y;      /* the vertical distance of the line */
	dxabs=abs(dx);
	dyabs=abs(dy);
	sdx=SIGN(dx);
	sdy=SIGN(dy);
	x=dyabs>>1;
	y=dxabs>>1;
	px=A.x;
	py=A.y;


	if (dxabs>=dyabs) /* the line is more horizontal than vertical */
	{
		for(i=0;i<dxabs;i++)
		{
			y+=dyabs;
			if (y>=dxabs)
			{
				y-=dxabs;
				py+=sdy;
			}
			px+=sdx;
			if (py < TEX_SIZE && py >= 0 && px < TEX_SIZE && px >= 0 )
			{
				ColorizePixel(&image[py][px],255,255,255);
			}
		}
	}
	else /* the line is more vertical than horizontal */
	{
		for(i=0;i<dyabs;i++)
		{
			x+=dxabs;
			if (x>=dyabs)
			{
				x-=dyabs;
				px+=sdx;
			}
			py+=sdy;
			if (py < TEX_SIZE && py >= 0 && px < TEX_SIZE && px >= 0 )
			{
				ColorizePixel(&image[py][px],255,255,255);
			}
		}
	}
}


void DrawCurve(Vertex A, Vertex B, Vertex C, Vertex D)
{
	int POINTS = 0;
	int DIV = 10;
	Vertex tmpA,tmpB;
	int i;
	double dt,t;
	double a,b;
	int X,Y;

	POINTS += sqrt(pow(A.x-B.x,2)+pow(A.y-B.y,2)) + sqrt(pow(B.x-C.x,2)+pow(B.y-C.y,2)) + sqrt(pow(C.x-D.x,2)+pow(C.y-D.y,2));

	dt = 1.0/(double)POINTS;
	createVertex(&tmpA,A.x,A.y);
	for (i = 0,t = 0; t < 1; t+=dt, ++i)
	{
		a = t;
		b = 1 - t;

		X = A.x * pow(b,3) + 3*B.x*pow(b,2)*a + 3*C.x*b*pow(a,2) + D.x*pow(a,3)-0.5;
		Y = A.y * pow(b,3) + 3*B.y*pow(b,2)*a + 3*C.y*b*pow(a,2) + D.y*pow(a,3)-0.5;
		createVertex(&tmpB,X,Y);
		DrawLine(tmpA,tmpB);
		createVertex(&tmpA,X,Y);
	}
}

void DrawImage(Vertex *field)
{
	int i,j,X,Y;
	Vertex *tmpField = (Vertex*) malloc ((endIndex1+1)*sizeof(Vertex));

	for (i = 0; i < endIndex1; i++)
	{
		X = Y = 0;
		
		X += matrix2d[0][0]*field[i].x + matrix2d[0][1]*field[i].y + matrix2d[0][2]*field[i].w; 
		Y += matrix2d[1][0]*field[i].x + matrix2d[1][1]*field[i].y + matrix2d[1][2]*field[i].w; 
		createVertex(&(tmpField[i]),X,Y);
	}

	for (i = 0; i < endIndex1-1; i += 3) 
	{
		DrawCurve(tmpField[i],tmpField[i+1],tmpField[i+2],tmpField[i+3]);
	}
	free(tmpField);
}

void DrawInitImage (Vertex *field)
{
	Vertex *tmpField = (Vertex*) malloc ((endIndex1+1)*sizeof(Vertex));
	int i,X,Y;
	matrix2d[0][0] = initMatrix[0][0] = width;
	matrix2d[1][1] = initMatrix[1][1] = heigth;
	for (i = 0; i < endIndex1; i++)
	{
		X = Y = 0;
		
		X += initMatrix[0][0]*field[i].x + initMatrix[0][1]*field[i].y + initMatrix[0][2]*field[i].w; 
		Y += initMatrix[1][0]*field[i].x + initMatrix[1][1]*field[i].y + initMatrix[1][2]*field[i].w; 
		createVertex(&(tmpField[i]),X,Y);
	}
	for (i = 0; i < endIndex1-1; i += 3) 
	{
		DrawCurve(tmpField[i],tmpField[i+1],tmpField[i+2],tmpField[i+3]);
	}	
	free(tmpField);
}

void GenerateImage()
{
	ClearImage();
	DrawImage(vertexField);
}

void loadCoordinates(char *path)
{
	char move; // position move
	char mode; // absolute, relative
	float x,y; // coordinates
	float baseX,baseY; // base coordinates after Move
	int i = 0;
	int vertexer = 0;
	int count;
	float wMax=INT_MIN,hMax=INT_MIN;
	float wMin=INT_MAX,hMin=INT_MAX;
	float tmpWidth,tmpHeigth;
	float middleW,middleH;
	FILE *f = fopen(path,"rb");
	
	fscanf(f,"%d ",&count);
	++count;

	while (fscanf(f,"%c",&move) && count>0) 
	{		
		if (move=='M')
		{
			fscanf(f," %f %f", &baseX, &baseY);
			createVertex(&vertexField[i],baseX,AXIS_SWAP(baseY));
			if (baseX>wMax) wMax = baseX;
			if (baseX<wMin) wMin = baseX;
			if (baseY>hMax) hMax = baseY;
			if (baseY<hMin) hMin = baseY;
			++i;
		}

		fscanf(f," %c",&mode);
		while (fscanf(f,"%f,%f ",&x,&y) && --count>=0)
		{
			++vertexer;
			if (mode=='c')
			{
				createVertex(&vertexField[i],baseX + x,AXIS_SWAP(baseY + y));
				if (vertexer % 3 == 0)
				{
					baseX += x;
					baseY += y;
					if (baseX>wMax) wMax = baseX;
					if (baseX<wMin) wMin = baseX;
					if (baseY>hMax) hMax = baseY;
					if (baseY<hMin) hMin = baseY;
				}
			}
			
			if (mode=='C')
			{
				createVertex(&vertexField[i],x,y);
			}
			++i;
		}
	}
	endIndex1 = i-1;
	fclose(f);
	tmpWidth = abs(wMax) + abs(wMin);
	tmpHeigth = abs(hMax) + abs(hMin);
	middleW = tmpWidth/2;
	middleH = tmpHeigth/2;

	for (i = 0; i < endIndex1; i++)
	{
		vertexField[i].x = (vertexField[i].x - (middleW))/tmpWidth;
		vertexField[i].y = (vertexField[i].y - (middleH))/tmpHeigth;
	}
	width = tmpWidth;
	heigth = tmpHeigth;
}

// Initialize OpenGL state
void init() {
	// load image from file
	loadCoordinates("L.svg");
	DrawInitImage(vertexField);
	// Texture setup
    glEnable(GL_TEXTURE_2D);
    glGenTextures( 1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    // Other
    glClearColor(0,0,0,0);
    gluOrtho2D(-1,1,-1,1);
    glLoadIdentity();
    glColor3f(1,1,1);
}

// Generate and display the image.
void display() {
    // Call user image generation
    GenerateImage();
    // Copy image to texture memory
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEX_SIZE, TEX_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    // Clear screen buffer
    glClear(GL_COLOR_BUFFER_BIT);
    // Render a quad
    glBegin(GL_QUADS);
        glTexCoord2f(1,0); glVertex2f(1,-1);
        glTexCoord2f(1,1); glVertex2f(1,1);
        glTexCoord2f(0,1); glVertex2f(-1,1);
        glTexCoord2f(0,0); glVertex2f(-1,-1);
    glEnd();
    // Display result
    glFlush();
    glutPostRedisplay();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	switch(key)
    {
	case 's' :
		transformations = SCALE;
		dxt=dyt=0;
		break;
	case 'r' :
		transformations = ROTATE;
		dxt=dyt=0;
		break;
	case 't' :
		transformations = TRANSLATE;
		dxt=dyt=0;
		break;
	case '0' :
		reset();
		break;
	case 'q' :
        exit(0);
        break;
	}
}

void specialKeyboard(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP :
		if (dyt < 0) 
		{
			dyt = 0;
			break;
		}
		dyt+=5;
		break;
	case GLUT_KEY_DOWN :
		if (dyt > 0) 
		{
			dyt = 0;
			break;
		}
		dyt-=5;
		break;
	case GLUT_KEY_LEFT :
		if (dxt > 0) 
		{
			dxt = 0;
			break;
		}
		dxt-=5;
		angle+=0.01;
		break;
	case GLUT_KEY_RIGHT :
		if (dxt < 0) 
		{
			dxt = 0;
			break;
		}
		dxt+=5;
		angle-=0.01;
		break;
	default:
		break;
	}
}


void idle() 
{
	switch (transformations)
	{
	case SCALE : scale(); break;
	case ROTATE : rotate(); break;
	case TRANSLATE : translate(); break;
	}
}


// Main entry function
int main(int argc, char ** argv) {
    // Init GLUT
	setbuf(stdout,NULL);
    glutInit(&argc, argv);
    glutInitWindowSize(TEX_SIZE, TEX_SIZE);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutCreateWindow("OpenGL Window");
    // Set up OpenGL state
    init();
    // Run the control loop
    glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutIdleFunc(idle);
    glutMainLoop();
    return EXIT_SUCCESS;
}

