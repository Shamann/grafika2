#define TEX_SIZE 512

typedef struct 
{
	GLubyte red;
	GLubyte green;
	GLubyte blue;
} Pixel;

typedef struct 
{
	float x,y,z,w;
} Vertex;

Vertex createVertex(float x, float y)
{
	Vertex *P = (Vertex*) malloc(sizeof(Vertex));
	P->x = x;
	P->y = y;
	P->z = 0;
	P->w = 1;
	return *P;
}

void createVertex(Vertex *P, float x, float y)
{
	P->x = x;
	P->y = y;
	P->z = 0;
	P->w = 1;
}

void createVertex(Vertex *P, float x, float y, float w)
{
	P->x = x;
	P->y = y;
	P->z = 0;
	P->w = w;
}